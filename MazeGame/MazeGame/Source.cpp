#include <iostream>
#include <stdio.h>      // printf, scanf, puts
#include <stdlib.h>     // srand, rand
#include <time.h>       // time
#include <conio.h>	    // _getch
#include <stdlib.h>		// system ("CLS")


using namespace std;

void display(char maze[64][64],int maxX, int maxY){ // I'm not proud of this
	cout << "+"; //Prints top of the board
	for (int x = 0; x < (maxX); x++) {
		cout << "-";
	}
	cout << "+" << endl;
	for (int x = 0; x < (maxY); x++) { //Prints the grid!
		cout << "|";
		for (int y = 0; y < (maxX); y++) { 
			cout << maze[x][y]; //Each element in the array is displayed between max and min
		}
		cout << "|" << endl;
	}
	cout << "+";
	for (int x = 0; x < (maxX); x++) {
		cout << "-";
	}
	cout << "+" << endl;
}
int playerMove(char maze[64][64], int& playerY, int& playerX) {
	char move = _getch(); //Gets user inpunt
	std::cin.sync();
	int sugMoveY = playerY, sugMoveX = playerX;
	if (move == 'w') { //Could use Case //Assignes Move to Player
		sugMoveY = playerY - 1; //Up
	}
	else if (move == 'a') {
		sugMoveX = playerX - 1; //Right
	}
	else if (move == 's') {
		sugMoveY = playerY + 1; //Down
	}
	else if (move == 'd') {
		sugMoveX = playerX + 1; //Left
	}

	/*else { 
		cout << "Invalid Input: W A S D" << endl;
		return(maze, playerY, playerX);
	}
	cout << maze[playerY][playerX];
	
	/*if (maze[sugMoveY][sugMoveX] == 'E') {
		cout << "Victory!" << endl;
		return 0;
	}*/

	if (maze[sugMoveY][sugMoveX] == '#') { //Makes sure move is valid
		cout << "Invalid Move: Hitting Wall" << endl;
		cin.ignore(); //Waits
		return(maze, playerY, playerX);
	}
	maze[playerY][playerX] = ' '; //This is horrible
	maze[sugMoveY][sugMoveX] = 'O'; //Updates positon
	playerX = sugMoveX;
	playerY = sugMoveY;
	return(maze, playerY, playerX);
};

char maze[64][64] = {
{'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', },
{'#', 'O', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', },
{'#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', },
{'#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', },
{'#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', '#', },
{'#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', '#', '#', '#', ' ', ' ', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', },
{'#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', ' ', ' ', '#', '#', '#', '#', '#', '#', '#', ' ', ' ', '#', },
{'#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ', '#', ' ', ' ', '#', ' ', ' ', ' ', ' ', ' ', ' ', ' ', 'E', '#', },
{'#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', '#', }, }; //Can be changed to depending size
int x, y,
maxX = 31,
maxY = 21,
playerX = 1,
playerY = 1; //Inistialises the maze of size x and y

int main() {
	while (playerY != 19 || playerX != 29) { //Untill end
		display(maze, maxX, maxY); //Display the maze
		display(maze, maxX, maxY); //Display the maze
		playerMove(maze, playerY, playerX); //Get the player to move
		system("CLS"); //Clears window
	}

	//For future use
	/*for (int x = 0; x < (maxY); x++) {
		for (int y = 0; y < (maxX); y++) {
			maze[x][y] = '#';
		}
	} */


// For future use
/* int pickDirection;
	srand(time(NULL));
	int randNum = (rand() % 4);
	int i = 1;
	srand(time(NULL));
	cout << rand() << endl;
	srand(i);
	srand(time(NULL));
	cout << rand() << endl;
	i++;
	srand(i);
	cout << rand() << endl;
	display(maze,maxX,maxY);
	
	*/
	return 0;
}



